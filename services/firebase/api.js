import {
  firebaseAuth,
  firebaseFirestore,
  firebaseStorage
} from "./setup";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut
} from "firebase/auth";

import {
  setDoc,
  doc,
  getDoc,
  updateDoc,
  collection
} from "firebase/firestore";

import {
  ref,
  uploadBytes,
  getDownloadURL
} from "firebase/storage";

let urlImagenEnStorage = '';

export async function registrarUsuario(correo, clave) {

  const usuario = await createUserWithEmailAndPassword(firebaseAuth, correo, clave)
    .then((userCredential) => {
      const usuarioRegistrado = userCredential.user;
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorCode);
      console.log(errorMessage);
    });
};

export async function iniciarSesionUsuario(correo, clave) {
  const usuario = await signInWithEmailAndPassword(firebaseAuth, correo, clave)
    .then((userCredential) => {
      const usuarioRegistrado = userCredential.user;
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorCode);
      console.log(errorMessage);
    });
};


export async function cerrarSesionUsuario() {
  signOut(firebaseAuth).then(() => {
  }).catch((error) => {
    console.error("No se pudo cerrar sesión: ", error);
  });
};

export function crearLibretasPorDefecto(id_usuario) {
  añadirLibreta(id_usuario, 'Notas');
  añadirLibreta(id_usuario, 'Importantes');
  añadirLibreta(id_usuario, 'Favoritos');
};

export async function añadirLibreta(id_usuario, nombreLibreta) {
  try {
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${nombreLibreta}`);
    var arregloNotas = [];
    const informacionNota = {
      titulo: `Bienvenido a ${nombreLibreta}`,
      texto: `Hola, bienvenido a la libreta ${nombreLibreta}`,
      imagen_url: ''
    };

    arregloNotas.push(informacionNota);
    await setDoc(documentoReferencia, {
      notas: [...arregloNotas]
    });
  } catch (error) {
    console.error("Error adding document: ", error);
  }
};


export async function obtenerNotasPorLibreta(id_usuario, id_libreta) {
  try {
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);
    const documento = await getDoc(documentoReferencia);

    if (documento.exists()) {
      const notasDocumento = documento.data();
      return notasDocumento.notas;
    } else {
      return null;
    }
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function añadirNota(id_usuario, id_libreta, nota) {
  try {
    var arregloNotas = await obtenerNotasPorLibreta(id_usuario, id_libreta);
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);

    const informacionNota = {
      titulo: nota.titulo,
      texto: nota.texto,
      imagen_url: urlImagenEnStorage
    };

    if (arregloNotas == null) {
      arregloNotas = [];
    }

    arregloNotas.push(informacionNota);
    await setDoc(documentoReferencia, {
      notas: [...arregloNotas]
    });
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function editarNota(id_usuario, id_libreta, id_nota, nota) {
  try {
    var arregloNotas = await obtenerNotasPorLibreta(id_usuario, id_libreta);
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);

    const informacionNota = {
      titulo: nota.titulo,
      texto: nota.texto,
      imagen_url: nota.imagen_url
    };

    arregloNotas.forEach((nota) => {
      if(nota.titulo == id_nota) {        
        nota.titulo = informacionNota.titulo;
        nota.texto = informacionNota.texto;
        nota.imagen_url = informacionNota.imagen_url;
      }
    });
    await updateDoc(documentoReferencia, {
      notas: [...arregloNotas]
    });
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function cargarArchivoEnStorage(id_usuario, e) {
  try {
    const archivoLocal = e.target.files[0];
    const storageReferencia = ref(firebaseStorage, `usuarios/imagenes_usuario_${id_usuario}/${archivoLocal.name}`);
    await uploadBytes(storageReferencia, archivoLocal);
    urlImagenEnStorage = await getDownloadURL(storageReferencia);    
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function eliminarNota(id_usuario, id_libreta, titulo_nota) {
  try {
    var arregloNotas = await obtenerNotasPorLibreta(id_usuario, id_libreta);
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);
    const arregloNotasConNotaEliminada = arregloNotas.filter((nota) => 
      nota.titulo != titulo_nota
    );
    await updateDoc(documentoReferencia, {
      notas: [...arregloNotasConNotaEliminada]
    });
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}