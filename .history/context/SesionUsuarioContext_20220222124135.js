import React, { useState, useEffect } from "react";
import { watcherSesionUsuarios } from ".services/firebase/watcher";

const SesionUsuarioContext = React.createContext();
const { Provider } = SesionUsuarioContext;

function SesionUsuarioContextProvider(props) {
  const [usuario, setUsuario] = useState(null
  );
  useEffect(() => {
    watcherSesionUsuarios((user) => {
      if (user) {
        setUsuario(user);
      } else {
        setUsuario(user);
      }
    });
  }, []);
  return <Provider value={usuario}>{props.children}</Provider>;
}
export { SesionUsuarioContext, SesionUsuarioContextProvider };
