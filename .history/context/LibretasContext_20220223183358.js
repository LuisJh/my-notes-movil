import React, { useState, useEffect, useContext } from "react";
import { watcherLibretasUsuario } from "../services/firebase/watcher";
import { SesionUsuarioContext } from './SesionUsuarioContext';

const LibretasContext = React.createContext();
const { Provider } = LibretasContext;

function LibretasContextProvider(props) {
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const [libretas, setLibretas] = useState(null);
  useEffect(() => {
    sesionUsuarioContext ? () : ();
    watcherLibretasUsuario((libretas) => {
      setLibretas(libretas);
    }, sesionUsuarioContext);
  }, []);
  return <Provider value={libretas}>{props.children}</Provider>;
}
export { LibretasContext, LibretasContextProvider };
