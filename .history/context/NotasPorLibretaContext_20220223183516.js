import React, { useState, useEffect, useContext } from "react";
import { watcherNotasPorLibreta } from "../services/firebase/watcher";
import { SesionUsuarioContext } from "./SesionUsuarioContext";
import { LibretaSeleccionadaContext } from "./LibretaSeleccionadaContext";

const NotasPorLibretaContext = React.createContext();
const { Provider } = NotasPorLibretaContext;

function NotasPorLibretaContextProvider(props) {
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const [libretaSeleccionada, setLibretaSeleccionada] = useContext(LibretaSeleccionadaContext);
  const [notas, setNotasPorLibreta] = useState([]);
  useEffect(() => {
    if() {
      
    }
    watcherNotasPorLibreta(
      (notas) => {
        setNotasPorLibreta(notas);
      },
      sesionUsuarioContext,
      libretaSeleccionada
    );
  }, [libretaSeleccionada]);
  return <Provider value={notas}>{props.children}</Provider>;
}
export { NotasPorLibretaContext, NotasPorLibretaContextProvider };
