import { StatusBar } from 'expo-status-bar';
import { useEffect. useContext } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
// import { Button } from 'react-native-web';
import { iniciarSesionUsuario, registrarUsuario, obtenerNotasPorLibreta } from './services/firebase/api'
import { SesionUsuarioContextProvider } from "./context/SesionUsuarioContext";


export default function App() {
  const usuario = {
    correo: "12345@gmail.com",
    clave: "123456"
  };
  onst sesionUsuarioContext = useContext(SesionUsuarioContext);
    console.log(sesionUsuarioContext);
  // useEffect(() => {
  //   const notas = obtenerNotasPorLibreta();
  // }, [input]);
  // console.log(iniciarSesionUsuario);
  return (
    <SesionUsuarioContextProvider>
    <View style={styles.container}>
      <Text>CHUPALO</Text>
      <Text>Correo: {usuario.correo}</Text>
      <Text>Clave: {usuario.clave}</Text>
      <StatusBar style="auto" />
      <Button title="Iniciar Sesion" onPress={() => {iniciarSesionUsuario(usuario.correo, usuario.clave)}}/>
      {/* <Button title="Iniciar Sesion" onPress={() => alert('HOLA')}/> */}
    </View>
    </SesionUsuarioContextProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
