import { StatusBar } from 'expo-status-bar';
import { useEffect, useContext } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
// import { Button } from 'react-native-web';
import { iniciarSesionUsuario, registrarUsuario, obtenerNotasPorLibreta } from './services/firebase/api'
import { SesionUsuarioContextProvider } from "./context/SesionUsuarioContext";
import Usuario from './screens/Usuario';
import Libretas from './screens/Libretas';
import IniciarSesion from './screens/IniciarSesion';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#621FF7",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <Stack.Screen
        name="Usuario"
        component={Usuario}
        options={{ title: "Usuario"}}
      />
      <Stack.Screen
        name="Libretas"
        component={Libretas}
        options={{ title: "Libretas", headerLeft: () => 
        <Button title="Cerrar Sesión" onPress={() => alert('Cerrar Sesión')} />
        }}
      />
      <Stack.Screen
        name="IniciarSesion"
        component={IniciarSesion}
        options={{ title: "Iniciar Sesión" }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <SesionUsuarioContextProvider>
    <MyStack />
      </SesionUsuarioContextProvider>
  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
