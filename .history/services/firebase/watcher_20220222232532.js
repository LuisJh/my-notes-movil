import { firebaseAuth, firebaseFirestore } from "./setup";
import { onAuthStateChanged } from "firebase/auth";
import { doc, onSnapshot, collection, query } from "firebase/firestore";

export function watcherSesionUsuarios(callback) {
  const unsub = onAuthStateChanged(firebaseAuth, (user) => {
    if (user && !user.isAnonymous) {
      callback({
        id: user.uid,
        email: user.email,
      });
    } else {
      callback(null);
    }
  });
  return unsub;
}

export function watcherLibretasUsuario(callback, usuario) {
  const ref = query(
    collection(
      firebaseFirestore,
      `notas_usuarios/notas_usuario_${usuario.id}/Libretas`
    )
  );
  const unsub = onSnapshot(ref, (libretas) => {
    const libretasObtenidas = [];
    libretas.forEach((libreta) => {
      libretasObtenidas.push(libreta.id);
    });
    console.log('Antes del callback [LIBRETAS]: ' + libretasObtenidas);
    callback(libretasObtenidas);
  });
  return unsub;
}

export function watcherNotasPorLibreta(callback, usuario, id_libreta) {
  const unsub = onSnapshot(
    doc(
      firebaseFirestore,
      `notas_usuarios/notas_usuario_${usuario.id}/Libretas`,
      `${id_libreta}`
    ),
    (documento) => {
      const informacionObtenida = documento.data();
      console.log("Current data: ", informacionObtenida);
      console.log("Notas: ", informacionObtenida.tareas);
      console.log('Antes del callback [NOTAS]: ' + libretasObtenidas);
      callback(informacionObtenida);
    }
  );
  return unsub;
}
