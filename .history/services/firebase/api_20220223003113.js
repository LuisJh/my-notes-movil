import {
  firebaseAuth,
  firebaseFirestore,
  firebaseStorage
} from "./setup";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut
} from "firebase/auth";

import {
  setDoc,
  doc,
  getDoc,
  updateDoc,
  collection
} from "firebase/firestore";

import {
  ref,
  uploadBytes,
  getDownloadURL
} from "firebase/storage";

let urlImagenEnStorage = '';

export async function registrarUsuario(correo, clave) {

  const usuario = await createUserWithEmailAndPassword(firebaseAuth, correo, clave)
    .then((userCredential) => {
      const usuarioRegistrado = userCredential.user;
      console.log(`Usuario registrado con éxito ${usuario}`);
      console.log(`Usuario Registrado en firebase: ${usuarioRegistrado}`);
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorCode);
      console.log(errorMessage);
    });
};

export async function iniciarSesionUsuario(correo, clave) {

  console.log(`${correo} - ${clave}`);
  const usuario = await signInWithEmailAndPassword(firebaseAuth, correo, clave)
    .then((userCredential) => {
      const usuarioRegistrado = userCredential.user;
      console.log(userCredential);
      console.log(`El usuario ha iniciado sesión con éxito ${usuario}`);
      console.log(`El usuario ha iniciado sesión en firebase: ${usuarioRegistrado}`);
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorCode);
      console.log(errorMessage);
    });
};
// export async function iniciarSesionUsuario(e) {
//   e.preventDefault();
//   const correo = e.target.iniciarSesionCorreoUsuario.value;
//   const clave = e.target.iniciarSesionClaveUsuario.value;
//   console.log(`${correo} - ${clave}`);
//   const usuario = await signInWithEmailAndPassword(firebaseAuth, correo, clave)
//     .then((userCredential) => {
//       const usuarioRegistrado = userCredential.user;
//       console.log(`El usuario ha iniciado sesión con éxito ${usuario}`);
//       console.log(`El usuario ha iniciado sesión en firebase: ${usuarioRegistrado}`);
//     })
//     .catch((error) => {
//       const errorCode = error.code;
//       const errorMessage = error.message;
//       console.log(errorCode);
//       console.log(errorMessage);
//     });
// };

export async function cerrarSesionUsuario() {
  signOut(firebaseAuth).then(() => {
    console.log("Sesión Cerrada");
  }).catch((error) => {
    console.error("No se pudo cerrar sesión: ", error);
  });
};

export function crearLibretasPorDefecto(id_usuario) {
  añadirLibreta(id_usuario, 'Notas');
  añadirLibreta(id_usuario, 'Importantes');
  añadirLibreta(id_usuario, 'Favoritos');
};

export async function añadirLibreta(id_usuario, nombreLibreta) {
  try {
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${nombreLibreta}`);
    // console.log(`Notas: ${obtenerNotasPorLibreta(id_usuario, nombreLibreta)}`);
    var arregloNotas = [];
    console.log(`arregloNotas: ${arregloNotas}`);
    const informacionNota = {
      titulo: `Bienvenido a ${nombreLibreta}`,
      texto: `Hola, bienvenido a la libreta ${nombreLibreta}`
    };

    arregloNotas.push(informacionNota);
    await setDoc(documentoReferencia, {
      notas: [...arregloNotas]
    });
    console.log("Libreta añadida: " + nombreLibreta);
  } catch (error) {
    console.error("Error adding document: ", error);
  }
};

// export async function obtenerLibretasPorUsuario(id_usuario) {
//   try {
//     const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);
//     const documento = await getDoc(documentoReferencia);

//     if (documento.exists()) {
//       const notasDocumento = documento.data();
//       console.log(notasDocumento.notas);
//       return notasDocumento.notas;
//     } else {
//       console.log("Return null");
//       return null;
//     }
//   } catch (error) {
//     console.error("Error adding document: ", error);
//   }
// }

// export async function obtenerLibretasPorUsuario(callback, usuario) {
//   const ref = query(
//     collection(
//       firebaseFirestore,
//       `notas_usuarios/notas_usuario_${usuario.id}/Libretas`
//     )
//   );
//   const unsub = await onSnapshot(ref, (libretas) => {
//     const libretasObtenidas = [];
//     libretas.forEach((libreta) => {
//       libretasObtenidas.push(libreta.id);
//     });
//     callback(libretasObtenidas);
//   });
//   return unsub;
// }

export async function existeLibretas(id_usuario) {
  try {
    const coleccionReferencia = collection(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`);
    // const coleccion = await getCollection(coleccionReferencia);
    console.log(coleccionReferencia);

    if (coleccionReferencia.exists()) {
      // const notasDocumento = coleccion.data();
      console.log('Existe Colección');
      // return notasDocumento.notas;
    } else {
      console.log("No existe Colección");
      // return null;
    }
  } catch (error) {
    console.error("Error adding document: ", error);
  }
};

export async function obtenerNotasPorLibreta(id_usuario, id_libreta) {
  try {
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);
    const documento = await getDoc(documentoReferencia);

    if (documento.exists()) {
      const notasDocumento = documento.data();
      console.log(notasDocumento.notas);
      return notasDocumento.notas;
    } else {
      console.log("Return null");
      return null;
    }
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function añadirNota(id_usuario, id_libreta, e) {
  try {
    e.preventDefault();
    const titulo = e.target.tituloNota.value;
    const texto = e.target.textoNota.value;
    var arregloNotas = await obtenerNotasPorLibreta(id_usuario, id_libreta);
    console.log(arregloNotas);
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);

    const informacionNota = {
      titulo: titulo,
      texto: texto,
      imagen_url: urlImagenEnStorage
    };

    if (arregloNotas == null) {
      arregloNotas = [];
    }

    arregloNotas.push(informacionNota);
    await setDoc(documentoReferencia, {
      notas: [...arregloNotas]
    });
    console.log("Nota añadida: " + informacionNota.titulo);
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function editarNota(id_usuario, id_libreta, id_nota, nota) {
  try {
    var arregloNotas = await obtenerNotasPorLibreta(id_usuario, id_libreta);
    console.log(arregloNotas);
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);

    const informacionNota = {
      titulo: nota.titulo,
      texto: texto,
      imagen_url: url
    };

    arregloNotas.foreEch((nota) => {
      console.log(nota);
      if(nota.titulo == id_nota) {
        nota = informacionNota;
      }
    });
    await updateDoc(documentoReferencia, {
      notas: [...arregloNotas]
    });
    console.log("Nota actualizada: " + id_nota);
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function cargarArchivoEnStorage(id_usuario, e) {
  try {
    const archivoLocal = e.target.files[0];
    const storageReferencia = ref(firebaseStorage, `usuarios/imagenes_usuario_${id_usuario}/${archivoLocal.name}`);
    await uploadBytes(storageReferencia, archivoLocal);
    urlImagenEnStorage = await getDownloadURL(storageReferencia);

    console.log("Imagen subida a Storgae con éxito: " + archivoLocal.name);
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}

export async function eliminarNota(id_usuario, id_libreta, titulo_nota) {
  try {
    console.log(id_libreta);
    var arregloNotas = await obtenerNotasPorLibreta(id_usuario, id_libreta);
    console.log(arregloNotas);
    console.log(titulo_nota);
    const documentoReferencia = doc(firebaseFirestore, `notas_usuarios/notas_usuario_${id_usuario}/Libretas`, `${id_libreta}`);
    const arregloNotasConNotaEliminada = arregloNotas.filter((nota) => 
      nota.titulo != titulo_nota
    );
    console.log(arregloNotasConNotaEliminada);
    await updateDoc(documentoReferencia, {
      notas: [...arregloNotasConNotaEliminada]
    });
    console.log("Nota eliminada con éxito: " + titulo_nota);
  } catch (error) {
    console.error("Error adding document: ", error);
  }
}