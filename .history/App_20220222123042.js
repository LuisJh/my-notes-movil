import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
// import { Button } from 'react-native-web';
import { iniciarSesionUsuario } from './services/firebase/api'

export default function App() {
  const usuario = {
    correo: 12345@gmail.com",
    clave: "123456"
  };
  // console.log(iniciarSesionUsuario);
  return (
    <View style={styles.container}>
      <Text>CHUPALO</Text>
      <Text>Correo: {usuario.correo}</Text>
      <Text>Clave: {usuario.clave}</Text>
      <StatusBar style="auto" />
      <Button title="Iniciar Sesion" onPress={() => {iniciarSesionUsuario(usuario.correo, usuario.clave)}}/>
      {/* <Button title="Iniciar Sesion" onPress={() => alert('HOLA')}/> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
