import React, { useContext, useState, useEffect } from "react";
import { Text, View } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";


const UserScreen = (props) => {
    const sesionUsuarioContext = useContext(SesionUsuarioContext);
    const [cargando, setCargando] = useState(true);
    console.log(sesionUsuarioContext);
    useEffect(() => {
      sesionUsuarioContext ? (setCargando(false)) : (console.log('Aun cargando'));
    }, []);
  return (
    <View>
      {
        cargando ? (
          sesionUsuarioContext ? (console.log('Hay usuario')) : (console.log('NO Hay usuario'))
        ) : (
          alert('cargando');
        )
      }
      {/* {sesionUsuarioContext ? (props.navigation.navigate("Libretas")) : (props.navigation.navigate("IniciarSesion"))} */}
    </View>
      );

};

export default UserScreen;
