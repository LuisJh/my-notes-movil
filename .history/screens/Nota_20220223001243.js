import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Button,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  TextInput,
} from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario, eliminarNota } from "../services/firebase/api";

const Notas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const libretaSeleccionada = props.route.params.libretaSeleccionada;
  const [nota, setNota] = useState(props.route.params.nota);
  console.log(sesionUsuarioContext);
  console.log(libretaSeleccionada);
  console.log(nota);

  const actualizarNota = () => {
    añadirLibreta(sesionUsuarioContext.id, libreta.titulo);
    props.navigation.navigate("Libretas");
  };

  const eliminarNotaFuncion = () => {
    añadirLibreta(sesionUsuarioContext.id, libreta.titulo);
    props.navigation.navigate("Libretas");
  };

  return (
    <ScrollView>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      {nota ? (
        <ScrollView style={styles.container}>
          <View style={styles.inputGroup}>
            <TextInput
              placeholder="Título"
              // onChangeText={(valor) =>
              //   setUsuario({ ...usuario, ["correo"]: valor })
              // }
              value={nota.titulo}
            />
          </View>

          <View style={styles.inputGroup}>
            <TextInput
              placeholder="Texto"
              // onChangeText={(valor) =>
              //   setUsuario({ ...usuario, ["clave"]: valor })
              // }
              value={nota.texto}
            />
          </View>

          <View style={styles.button}>
            <Button title="VER" onPress={() => console.log(nota)} />
            <Button
              title="Guardar Nota"
              onPress={() => registrarUsuario(usuario.correo, usuario.clave)}
            />
            <Button
              title="Eliminar Nota"
              onPress={() => registrarUsuario(usuario.correo, usuario.clave)}
            />
          </View>
        </ScrollView>
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )}
      ;
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});

export default Notas;
