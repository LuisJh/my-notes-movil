import React, { useContext } from "react";
import { Text, View, Button } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario } from '../services/firebase/api'

const Libretas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  }
    const sesionUsuarioContext = useContext(SesionUsuarioContext);
    console.log(sesionUsuarioContext);

  return (
    <View>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      <Text>
        Libretas
      </Text>
    </View>
  );
};

export default Libretas;
