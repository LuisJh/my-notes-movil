import React, { useContext } from "react";
import { Text, View } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";


const UserScreen = (props) => {
    const sesionUsuarioContext = useContext(SesionUsuarioContext);
    console.log(sesionUsuarioContext);

  return (
    <View>
      {sesionUsuarioContext ? (props.navigation.navigate("Libretas")) : (props.navigation.navigate("UsersList"))}
    </View>
      );

};

export default UserScreen;
