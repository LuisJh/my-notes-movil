import React, { useContext, useState, useEffect } from "react";
import { View, Button, ScrollView, ActivityIndicator, StyleSheet } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import {
  cerrarSesionUsuario,
  crearLibretasPorDefecto,
} from "../services/firebase/api";
import { watcherLibretasUsuario } from "../services/firebase/watcher";

const Libretas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const [libretas, setLibretas] = useState(null);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  console.log(sesionUsuarioContext);
  console.log(libretas);

  useEffect(() => {
    watcherLibretasUsuario((libretas) => {
      setLibretas(libretas);
    }, sesionUsuarioContext);
  }, []);

  return (
    <ScrollView>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      {libretas ? (
        libretas.length === 0 ? (
          crearLibretasPorDefecto(sesionUsuarioContext.id)
        ) : (
          libretas.map((libreta) => {
            return (
              <ListItem
                key={libreta}
                bottomDivider
                onPress={() => {
                  props.navigation.navigate("NotasPorLibreta", {
                    id_libreta: libreta,
                  });
                }}
              >
                <ListItem.Chevron />
                <Avatar
                  source={{
                    uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
                  }}
                  rounded
                />
                <ListItem.Content>
                  <ListItem.Title>{libreta}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            );
          })
        )
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )}
      ;
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});

export default Libretas;
