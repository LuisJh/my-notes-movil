import React, { useContext, useState, useEffect } from "react";
import { Text, View, Button } from "react-native";
import { ListItem } from "react-native-elements";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario } from '../services/firebase/api'

const Libretas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  }

  const [libretas, setLibretas] = useState(null);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
   console.log(sesionUsuarioContext);

   useEffect(() => {
    setLibretas();
  }, []);

  return (
    <View>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      <Text>
        Libretas
      </Text>
      {libretas ? (
            libretasContext.length === 0 ?(
                crearLibretasPorDefecto(sesionUsuarioContext.id)
            ):(
                <Container id='contenedorLibreta'>
                <Row style={{backgroundColor:'white'}}>
                    {
                        libretasContext.map(libreta => {
                            return(
                            <Col>
                                <Button style={{marginTop:'0.5em'}} variant="outline-success" onClick={()=>setLibretaSeleccionada(libreta)}>{libreta}</Button>
                            </Col>)
                        })
                    }
                            <Button variant="warning" onClick={handleShow} id='btnCrearLibreta'>
                                Crear libreta
                            </Button>
                            <Modal show={show} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Cree su nueva libreta</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form id='formularioNota'>
                                        <Form.Group id='tituloNota'>
                                            <Form.Label>Título</Form.Label>
                                            <Form.Control type="text" placeholder="Ingrese el título" id='tituloLibreta' />
                                        </Form.Group>
                                        <Button variant="success" id='btnCrearLibreta' onClick={()=>{añadirLibreta(sesionUsuarioContext.id, document.getElementById("tituloLibreta").value);handleClose()}}>
                                            Crear libreta
                                        </Button>
                                    </Form>
                                </Modal.Body>
                            </Modal>
                </Row>
                <Row>
                    <Col id='columnaContenedor'>
                    <NotasPorLibretaContextProvider>
                        <Notas setNota={setNota}/>
                    </NotasPorLibretaContextProvider>
                    </Col>
                    <Col>
                        {nota ?(
                            <PresentarNota nota = {nota} setNota={setNota}/>
                        ):(
                            <CrearNota />
                        )}
                    </Col>
                </Row>
            </Container>
            )
        ):(
            <h2>No se cargan las libretas</h2>
        )};
    </View>
  );
};

export default Libretas;

const UserScreen = (props) => {

  

  return (
    <ScrollView>
      <Button
        onPress={() => props.navigation.navigate("CreateUserScreen")}
        title="Create User"
      />
      {libretas.map((user) => {
        return (
          <ListItem
            key={user.id}
            bottomDivider
            onPress={() => {
              props.navigation.navigate("UserDetailScreen", {
                userId: user.id,
              });
            }}
          >
            <ListItem.Chevron />
            <Avatar
              source={{
                uri:
                  "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
              }}
              rounded
            />
            <ListItem.Content>
              <ListItem.Title>{user.name}</ListItem.Title>
              <ListItem.Subtitle>{user.email}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        );
      })}
    </ScrollView>
  );
};

export default UserScreen;
