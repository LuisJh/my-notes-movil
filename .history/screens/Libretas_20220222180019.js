import React, { useContext } from "react";
import { Text, View, Button } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario } from '../services/firebase/api'

const Libretas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  }
    const sesionUsuarioContext = useContext(SesionUsuarioContext);
    console.log(sesionUsuarioContext);

  return (
    <View>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      <Text>
        Libretas
      </Text>
    </View>
  );
};

export default Libretas;


import React, { useState, useEffect } from "react";
import { Button, StyleSheet } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";

import firebase from "../database/firebase";

const UserScreen = (props) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    firebase.db.collection("users").onSnapshot((querySnapshot) => {
      const users = [];
      querySnapshot.docs.forEach((doc) => {
        const { name, email, phone } = doc.data();
        users.push({
          id: doc.id,
          name,
          email,
          phone,
        });
      });
      setUsers(users);
    });
  }, []);

  return (
    <ScrollView>
      <Button
        onPress={() => props.navigation.navigate("CreateUserScreen")}
        title="Create User"
      />
      {users.map((user) => {
        return (
          <ListItem
            key={user.id}
            bottomDivider
            onPress={() => {
              props.navigation.navigate("UserDetailScreen", {
                userId: user.id,
              });
            }}
          >
            <ListItem.Chevron />
            <Avatar
              source={{
                uri:
                  "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
              }}
              rounded
            />
            <ListItem.Content>
              <ListItem.Title>{user.name}</ListItem.Title>
              <ListItem.Subtitle>{user.email}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        );
      })}
    </ScrollView>
  );
};

export default UserScreen;
