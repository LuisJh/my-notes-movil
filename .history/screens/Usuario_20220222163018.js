import React, { useContext, useState, useEffect } from "react";
import { Text, View } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";


const UserScreen = (props) => {
    const sesionUsuarioContext = useContext(SesionUsuarioContext);
    const [cargando, setCargando] = useState(true);
    console.log(sesionUsuarioContext);
    useEffect(() => {
      sesionUsuarioContext ? (setCargando(false)) : (console.log('Aun cargando'));
    }, [sesionUsuarioContext]);
  return (
    <View>
      {
        cargando ? (
          <View style={styles.loader}>
        <ActivityIndicator size="large" color="#9E9E9E" />
      </View>
          ) : (
          sesionUsuarioContext ? (props.navigation.navigate("Libretas")) : (props.navigation.navigate("IniciarSesion"))
        )
      }
    </View>
      );

};

export default UserScreen;
