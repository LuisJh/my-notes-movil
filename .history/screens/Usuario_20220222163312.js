import React, { useContext, useState, useEffect } from "react";
import { Text, View } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";


export default const UserScreen = (props) => {
    const sesionUsuarioContext = useContext(SesionUsuarioContext);
    const [cargando, setCargando] = useState(true);
    console.log(sesionUsuarioContext);
    useEffect(() => {
      sesionUsuarioContext ? (setCargando(false)) : (console.log('Aun cargando'));
    }, [sesionUsuarioContext]);
  return (
    <View>
      {
        cargando ? (
          <View style={styles.loader}>
        <ActivityIndicator size="large" color="#9E9E9E" />
      </View>
          ) : (
          sesionUsuarioContext ? (props.navigation.navigate("Libretas")) : (props.navigation.navigate("IniciarSesion"))
        )
      }
    </View>
      );

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});
