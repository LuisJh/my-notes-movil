import React, { useContext } from "react";
import {
  Button,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
} from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";


const UserScreen = (props) => {

  const usuarioDatosIniciales = {
    correo: "",
    clave: "",
  };

  const [usuario, setUsuario] = useUsuario(usuarioDatosIniciales);

  const handleChangeText = (value, name) => {
    setUsuario({ ...usuario, [name]: value });
  };

  const saveNewUser = async () => {
    if (usuario.name === "") {
      alert("please provide a name");
    } else {

      try {
        await firebase.db.collection("users").add({
          name: usuario.name,
          email: usuario.email,
          phone: usuario.phone,
        });

        props.navigation.navigate("UsersList");
      } catch (error) {
        console.log(error)
      }
    }
  };
  
  return (
    <ScrollView style={styles.container}>

      {/* Email Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Correo electrónico"
          multiline={true}
          numberOfLines={4}
          onChangeText={(value) => handleChangeText(value, "email")}
          value={usuario.email}
        />
      </View>

      {/* Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="clave"
          onChangeText={(value) => handleChangeText(value, "phone")}
          value={usuario.phone}
        />
      </View>

      <View style={styles.button}>
        <Button title="Iniciar Sesión" onPress={() => saveNewUser()} />
      </View>
      <View style={styles.button}>
        <Button title="Registarse" onPress={() => alert('Regist')} />
      </View>
    </ScrollView>
  );
};

export default UserScreen;
