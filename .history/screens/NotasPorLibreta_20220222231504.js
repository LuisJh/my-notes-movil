import React, { useContext, useState, useEffect } from "react";
import { View, Button, ScrollView, ActivityIndicator, StyleSheet } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import {
  cerrarSesionUsuario,
} from "../services/firebase/api";
import { watcherNotasPorLibreta } from "../services/firebase/watcher";

const Notas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const [notas, setNotas] = useState(null);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  console.log(sesionUsuarioContext);
  console.log(notas);

  watcherNotasPorLibreta(
      (notas) => {
        setNotasPorLibreta(notas);
      },
      sesionUsuarioContext,
      libretaSeleccionada
    );

  return (
    <ScrollView>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      {notas ? (
        notas.length === 0 ? (
          crearNotasPorDefecto(sesionUsuarioContext.id)
        ) : (
          notas.map((libreta) => {
            return (
              <ListItem
                key={libreta}
                bottomDivider
                onPress={() => {
                  // props.navigation.navigate("UserDetailScreen", {
                  //   libretaId: libreta.id,
                  // });
                  alert("Haz hecho click en: " + libreta);
                }}
              >
                <ListItem.Chevron />
                <Avatar
                  source={{
                    uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
                  }}
                  rounded
                />
                <ListItem.Content>
                  <ListItem.Title>{libreta}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            );
          })
        )
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )}
      ;
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});

export default Notas;
