import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Button,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  TextInput,
} from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario, eliminarNota, editarNota } from "../services/firebase/api";
import { LibretaSeleccionadaContext } from "./context/LibretaSeleccionadaContext";

const Notas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  // const libretaSeleccionada = props.route.params.libretaSeleccionada;
  const [libretaSeleccionada, setLibretaSeleccionada] = useContext(LibretaSeleccionadaContext);
  const [nota, setNota] = useState(props.route.params.nota);
  const id_nota = props.route.params.nota.titulo;
  console.log(sesionUsuarioContext);
  console.log(libretaSeleccionada);
  console.log(nota);

  const actualizarNota = () => {
    editarNota(sesionUsuarioContext.id, libretaSeleccionada, id_nota, nota);
    props.navigation.navigate("NotasPorLibreta");
  };

  const eliminarNotaFuncion = () => {
    eliminarNota(sesionUsuarioContext.id, libretaSeleccionada, nota.titulo);
    props.navigation.navigate("NotasPorLibreta");
  };

  return (
    <ScrollView>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      {nota ? (
        <ScrollView style={styles.container}>
          <View style={styles.inputGroup}>
            <TextInput
              placeholder="Título"
              onChangeText={(valor) =>
                setNota({ ...nota, ["titulo"]: valor })
              }
              value={nota.titulo}
            />
          </View>

          <View style={styles.inputGroup}>
            <TextInput
              placeholder="Texto"
              onChangeText={(valor) =>
                setNota({ ...nota, ["texto"]: valor })
              }
              value={nota.texto}
            />
          </View>

          <View style={styles.button}>
            <Button title="VER" onPress={() => console.log(nota)} />
            <Button
              title="Guardar Nota"
              onPress={() => actualizarNota()}
            />
            <Button
              title="Eliminar Nota"
              onPress={() => eliminarNotaFuncion()}
            />
          </View>
        </ScrollView>
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )}
      ;
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});

export default Notas;
