import React, { useContext, useState } from "react";
import {
  Button,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
} from "react-native";
import { iniciarSesionUsuario } from '../services/firebase/api'

const IniciarSesion = (props) => {

  const usuarioDatosIniciales = {
    correo: "",
    clave: "",
  };

  const [usuario, setUsuario] = useState(usuarioDatosIniciales);

  const handleChangeText = (atributo, valor) => {
    setUsuario({ ...usuario, [atributo]: valor });
  };

  const saveNewUser = async () => {
    if (usuario.name === "") {
      alert("please provide a name");
    } else {

      try {
        await firebase.db.collection("users").add({
          name: usuario.name,
          email: usuario.email,
          phone: usuario.phone,
        });

        props.navigation.navigate("UsersList");
      } catch (error) {
        console.log(error)
      }
    }
  };
  
  return (
    <ScrollView style={styles.container}>

      {/* Email Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Correo electrónico"
          multiline={true}
          numberOfLines={4}
          onChangeText={(valor) => setUsuario({ ...usuario, [atributo]: valor });}
          value={usuario.email}
        />
      </View>

      {/* Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Clave"
          onChangeText={(valor) => handleChangeText(valor, "clave")}
          value={usuario.phone}
          secureTextEntry={true}
        />
      </View>

      <View style={styles.button}>
        <Button title="Iniciar Sesión" onPress={() => iniciarSesionUsuario(usuario.correo, usuario.clave)} />
      </View>
      <View style={styles.button}>
        <Button title="Registarse" onPress={() => alert('Registrar')} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default IniciarSesion;
