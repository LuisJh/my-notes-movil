import React, { useContext, useState, useEffect } from "react";
import { View, Button, ScrollView, ActivityIndicator, StyleSheet } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import {
  cerrarSesionUsuario,
} from "../services/firebase/api";
import { watcherNotasPorLibreta } from "../services/firebase/watcher";

const Notas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const [notas, setNotasPorLibreta] = useState(null);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const libretaSeleccionada = props.route.params.libretaSeleccionada;
  const libretaSeleccionada = props.route.params.libretaSeleccionada;
  console.log(sesionUsuarioContext);
  console.log(libretaSeleccionada);
  console.log(notas);

  useEffect(() => {
    watcherNotasPorLibreta(
      (notas) => {
        setNotasPorLibreta(notas);
      },
      sesionUsuarioContext,
      libretaSeleccionada
    );
  }, []);

  return (
    <ScrollView>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      {notas ? (
        notas.length === 0 ? (
          <Text>Aún no tiene notas</Text>
        ) : (
          notas.map((nota) => {
            return (
              <ListItem
                key={nota.titulo}
                bottomDivider
                onPress={() => {
                  // props.navigation.navigate("UserDetailScreen", {
                  //   notaId: nota.id,
                  // });
                  alert("Haz hecho click en: " + nota.titulo);
                }}
              >
                <ListItem.Chevron />
                <Avatar
                  source={{
                    uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
                  }}
                  rounded
                />
                <ListItem.Content>
                  <ListItem.Title>{nota.titulo}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            );
          })
        )
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )}
      ;
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});

export default Notas;
