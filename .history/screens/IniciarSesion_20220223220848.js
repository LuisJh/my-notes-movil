import React, { useState } from "react";
import { Button, View, StyleSheet, TextInput, ScrollView, Text } from "react-native";
import { iniciarSesionUsuario } from '../services/firebase/api'

const IniciarSesion = (props) => {

  const usuarioDatosIniciales = {
    correo: "",
    clave: "",
  };
  const [usuario, setUsuario] = useState(usuarioDatosIniciales);
  
  return (
    <ScrollView style={styles.container}>
      <View>
        <Text style={styles.text}>Inicio Sesión</Text>
      </View>
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Correo electrónico"
          onChangeText={(valor) => setUsuario({ ...usuario, ["correo"]: valor })}
        />
      </View>

      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Contraseña"
          onChangeText={(valor) => setUsuario({ ...usuario, ["clave"]: valor })}
          secureTextEntry={true}
        />
      </View>

      <View style={styles.button}>
        <Button title="Iniciar Sesión" color="#198754" onPress={() => iniciarSesionUsuario(usuario.correo, usuario.clave)} />
      </View>
      <View style={styles.button}>
        <Button title="Registrarse" color="#0d6efd" onPress={() => props.navigation.navigate("RegistrarUsuario")} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  button: {
    margin: 6,
  },
  text:{
    fontSize: 30,
    marginBottom:20,
  },
});

export default IniciarSesion;
