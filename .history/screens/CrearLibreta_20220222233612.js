import React, { useState } from "react";
import {
  Button,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
} from "react-native";
import { registrarLibreta } from '../services/firebase/api'

const CrearLibreta = (props) => {

  const libretaDatosIniciales = {
    correo: "",
    clave: "",
  };
  const [libreta, setLibreta] = useState(libretaDatosIniciales);
  
  return (
    <ScrollView style={styles.container}>

      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Titúlo"
          onChangeText={(valor) => setLibreta({ ...libreta, ["correo"]: valor })}
        />
      </View>

      <View style={styles.button}>
        <Button title="VER" onPress={() => console.log(libreta)} />
        <Button title="Crear Libreta" onPress={() => registrarLibreta(libreta.correo, libreta.clave)} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default CrearLibreta;
