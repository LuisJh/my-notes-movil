import React, { useState, useContext } from "react";
import {
  Button,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
} from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { añadirNota } from '../services/firebase/api'

const CrearNota = (props) => {

  const notaDatosIniciales = {
    titulo: "",
    texto: "",
    imagen_url: "",
  };
  const [nota, setNota] = useState(notaDatosIniciales);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  
  const crearNota = () => {
    añadirNota(sesionUsuarioContext.id, nota.titulo);
    props.navigation.navigate("Notas");
  };

  return (
    <ScrollView style={styles.container}>

      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Titúlo"
          onChangeText={(valor) => setNota({ ...nota, ['titulo']: valor })}
        />
      </View>

      <View style={styles.button}>
        <Button title="VER" onPress={() => console.log(nota)} />
        <Button title="Crear Nota" onPress={() => crearNota()} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default CrearNota;
