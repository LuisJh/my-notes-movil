import React, { useContext, useState, useEffect } from "react";
import { Text, View, Button, ScrollView } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
// import { ScrollView } from "react-native-gesture-handler";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario, crearLibretasPorDefecto } from "../services/firebase/api";
import { watcherLibretasUsuario } from '../services/firebase/watcher'

const Libretas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const [libretas, setLibretas] = useState(null);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  console.log(sesionUsuarioContext);
  console.log(sesionUsuarioContext);

  useEffect(() => {
    watcherLibretasUsuario((libretas) => {
      setLibretas(libretas);
    }, sesionUsuarioContext);
  }, []);

  return (
    <View>
      <Button title="Cerrar Sesión" onPress={() => cerrarSesión()} />
      <Text>Libretas</Text>
      {libretas ? (
        libretas.length === 0 ? (
          crearLibretasPorDefecto(sesionUsuarioContext.id)
        ) : (
          libretas.map((libreta) => {
            return (
              <ListItem
                key={libreta}
                bottomDivider
                onPress={() => {
                  // props.navigation.navigate("UserDetailScreen", {
                  //   libretaId: libreta.id,
                  // });
                  alert("Haz hecho click en: " + libreta);
                }}
              >
                <ListItem.Chevron />
                <Avatar
                  source={{
                    uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
                  }}
                  rounded
                />
                <ListItem.Content>
                  <ListItem.Title>{libreta}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            );
          })
        )
      ) : (
        <Text>No se cargan las notas</Text>
      )}
      ;
    </View>
  );
};

export default Libretas;
