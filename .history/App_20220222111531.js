import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
// import { Button } from 'react-native-web';
import { iniciarSesionUsuario } from './services/firebase/api'

export default function App() {
  return (
    <View style={styles.container}>
      <Text>CHUPALO</Text>
      <StatusBar style="auto" />
      <Button title="Iniciar Sesion" onPress={() => {inic}}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
