import { StyleSheet } from "react-native";
import { SesionUsuarioContextProvider } from "./context/SesionUsuarioContext";
import Usuario from "./screens/Usuario";
import Libretas from "./screens/Libretas";
import NotasPorLibreta from "./screens/NotasPorLibreta";
import IniciarSesion from "./screens/IniciarSesion";
import RegistrarUsuario from "./screens/RegistrarUsuario";
import CrearLibreta from "./screens/CrearLibreta";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#621FF7",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <Stack.Screen
        name="Usuario"
        component={Usuario}
        options={{ title: "Usuario" }}
      />
      <Stack.Screen
        name="Libretas"
        component={Libretas}
        options={{ title: "Libretas" }}
      />
      <Stack.Screen
        name="CrearLibreta"
        component={CrearLibreta}
        options={{ title: "Crear Libreta" }}
      />
      <Stack.Screen
        name="Nota"
        component={Nota}
        options={{ title: "Crear Libreta" }}
      />
      <Stack.Screen
        name="NotasPorLibreta"
        component={NotasPorLibreta}
        options={{ title: "Notas" }}
      />
      <Stack.Screen
        name="IniciarSesion"
        component={IniciarSesion}
        options={{ title: "Iniciar Sesión" }}
      />
      <Stack.Screen
        name="RegistrarUsuario"
        component={RegistrarUsuario}
        options={{ title: "Registrar Usuario" }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <SesionUsuarioContextProvider>
        <MyStack />
      </SesionUsuarioContextProvider>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
