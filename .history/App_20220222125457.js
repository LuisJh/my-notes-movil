import { StatusBar } from 'expo-status-bar';
import { useEffect, useContext } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
// import { Button } from 'react-native-web';
import { iniciarSesionUsuario, registrarUsuario, obtenerNotasPorLibreta } from './services/firebase/api'
import { SesionUsuarioContext, SesionUsuarioContextProvider } from "./context/SesionUsuarioContext";
import Usuario from './screens/Usuario';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

export default function App() {
  const usuario = {
    correo: "12345@gmail.com",
    clave: "123456"
  };
  
  // useEffect(() => {
  //   const notas = obtenerNotasPorLibreta();
  // }, [input]);
  // console.log(iniciarSesionUsuario);
  return (
    <SesionUsuarioContextProvider>
    {/* <View style={styles.container}>
      <Text>CHUPALO</Text>
      <Text>Correo: {usuario.correo}</Text>
      <Text>Clave: {usuario.clave}</Text>
      <StatusBar style="auto" />
      <Button title="Iniciar Sesion" onPress={() => {iniciarSesionUsuario(usuario.correo, usuario.clave)}}/>
    </View> */}
    <Usuario />
    </SesionUsuarioContextProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
