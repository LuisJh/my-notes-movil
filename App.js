import { StyleSheet } from "react-native";
import { SesionUsuarioContextProvider } from "./context/SesionUsuarioContext";
import Usuario from "./screens/Usuario";
import Libretas from "./screens/Libretas";
import NotasPorLibreta from "./screens/NotasPorLibreta";
import IniciarSesion from "./screens/IniciarSesion";
import RegistrarUsuario from "./screens/RegistrarUsuario";
import CrearLibreta from "./screens/CrearLibreta";
import Nota from "./screens/Nota";
import CrearNota from "./screens/CrearNota";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { LibretaSeleccionadaContextProvider } from "./context/LibretaSeleccionadaContext";
import { LibretasContextProvider } from "./context/LibretasContext";
import { NotasPorLibretaContextProvider } from "./context/NotasPorLibretaContext";

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#0d6efd",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <Stack.Screen
        name="Usuario"
        component={Usuario}
        options={{ title: "My Notes 📝" }}
      />
      <Stack.Screen
        name="CrearLibreta"
        component={CrearLibreta}
        options={{ title: "My Notes 📝" }}
      />

      <Stack.Screen
        name="Libretas"
        component={Libretas}
        options={{ title: "My Notes 📝" }}
      />
      <Stack.Screen
        name="Nota" component={Nota} options={{ title: "My Notes 📝" }}
      />
      <Stack.Screen
        name="CrearNota"
        component={CrearNota}
        options={{ title: "My Notes 📝" }}
      />
      <Stack.Screen
        name="NotasPorLibreta"
        component={NotasPorLibreta}
        options={{ title: "My Notes 📝" }}
      />
      <Stack.Screen
        name="IniciarSesion"
        component={IniciarSesion}
        options={{ title: "My Notes 📝" }}
      />
      <Stack.Screen
        name="RegistrarUsuario"
        component={RegistrarUsuario}
        options={{ title: "My Notes 📝" }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <SesionUsuarioContextProvider>
        <LibretasContextProvider>
          <LibretaSeleccionadaContextProvider>
            <NotasPorLibretaContextProvider>
              <MyStack />
            </NotasPorLibretaContextProvider>
          </LibretaSeleccionadaContextProvider>
        </LibretasContextProvider>
      </SesionUsuarioContextProvider>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
