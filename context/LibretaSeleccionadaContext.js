import React, { useState } from "react";

const LibretaSeleccionadaContext = React.createContext();
const { Provider } = LibretaSeleccionadaContext;

function LibretaSeleccionadaContextProvider(props) {
  const [libretaSeleccionada, setLibretaSeleccionada] = useState(null);
  return <Provider value={[libretaSeleccionada, setLibretaSeleccionada]}>{props.children}</Provider>;
}
export { LibretaSeleccionadaContext, LibretaSeleccionadaContextProvider };
