import React, { useState, useContext } from "react";
import { Button, View, StyleSheet, TextInput, ScrollView, Text } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { añadirNota } from '../services/firebase/api';
import { LibretaSeleccionadaContext } from "../context/LibretaSeleccionadaContext";

const CrearNota = (props) => {

  const notaDatosIniciales = {
    titulo: "",
    texto: "",
    imagen_url: "",
  };
  const [nota, setNota] = useState(notaDatosIniciales);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const [libretaSeleccionada, setLibretaSeleccionada] = useContext(LibretaSeleccionadaContext);

  const crearNota = () => {
    añadirNota(sesionUsuarioContext.id, libretaSeleccionada, nota);
    props.navigation.navigate("NotasPorLibreta");
  };

  return (
    <ScrollView style={styles.container}>
      <View>
        <Text style={styles.text}>Crear Nota</Text>
      </View>
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Título"
          onChangeText={(valor) => setNota({ ...nota, ['titulo']: valor })}
        />
      </View>

      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Contenido"
          onChangeText={(valor) => setNota({ ...nota, ['texto']: valor })}
        />
      </View>

      <View style={styles.button}>
        <Button color="#198754" title="Crear Nota" onPress={() => crearNota()} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  text:{
    fontSize: 30,
    marginBottom:20,
  },
});

export default CrearNota;
