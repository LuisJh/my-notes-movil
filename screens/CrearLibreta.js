import React, { useState, useContext } from "react";
import { Button, View, StyleSheet, TextInput, ScrollView, Text } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { registrarLibreta, añadirLibreta } from '../services/firebase/api'

const CrearLibreta = (props) => {

  const libretaDatosIniciales = {
    titulo: "",
  };
  const [libreta, setLibreta] = useState(libretaDatosIniciales);
  const sesionUsuarioContext = useContext(SesionUsuarioContext);

  const crearLibreta = () => {
    añadirLibreta(sesionUsuarioContext.id, libreta.titulo);
    props.navigation.navigate("Libretas");
  };

  return (
    <ScrollView style={styles.container}>
      <View>
        <Text style={styles.text}>Crear Libreta</Text>
      </View>
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Título"
          onChangeText={(valor) => setLibreta({ ...libreta, ['titulo']: valor })}
        />
      </View>

      <View style={styles.button}>
        <Button  title="Crear Libreta" onPress={() => crearLibreta()} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  text:{
    fontSize: 30,
    marginBottom:20,
  },
});

export default CrearLibreta;
