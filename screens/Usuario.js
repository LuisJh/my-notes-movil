import React, { useContext, useState, useEffect } from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";

const UserScreen = (props) => {
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const [cargando, setCargando] = useState(true);
  useEffect(() => {
    setCargando(false);
  }, []);
  return (
    <View>
      {cargando ? (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#0d6efd" />
        </View>
      ) : sesionUsuarioContext ? (
        props.navigation.navigate("Libretas")
      ) : (
        props.navigation.navigate("IniciarSesion")
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
});

export default UserScreen;
