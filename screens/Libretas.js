import React, { useContext, useState, useEffect } from "react";
import { View, Button, ScrollView, ActivityIndicator, StyleSheet, Text } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { LibretaSeleccionadaContext } from "../context/LibretaSeleccionadaContext";
import imagenLibreta from "../assets/imgLibreta.png";
import { LibretasContext } from "../context/LibretasContext";
import {cerrarSesionUsuario,crearLibretasPorDefecto,} from "../services/firebase/api";
import { watcherLibretasUsuario } from "../services/firebase/watcher";

const Libretas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };
  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const libretasContext = useContext(LibretasContext);
  const [libretaSeleccionada, setLibretaSeleccionada] = useContext(LibretaSeleccionadaContext);
  const seleccionarLibreta = (libreta) => {
    setLibretaSeleccionada(libreta);
    props.navigation.navigate("NotasPorLibreta");
  }
  return (
    <ScrollView>
      <View>
        <Text style={styles.text}>Libretas</Text>
      </View>
      <Button color="#198754" title="Crear Libreta" onPress={() => props.navigation.navigate("CrearLibreta")} />
      {libretasContext ? (
        libretasContext.length === 0 ? (
          crearLibretasPorDefecto(sesionUsuarioContext.id)
        ) : (
          libretasContext.map((libreta) => {
            return (
              <ListItem
                key={libreta}
                bottomDivider
                onPress={() => {
                  seleccionarLibreta(libreta);
                }}
              >
                <ListItem.Chevron />
                <Avatar
                  source={imagenLibreta}
                  rounded
                />
                <ListItem.Content>
                  <ListItem.Title>{libreta}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            );
          })
        )
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#0d6efd"/>
        </View>
      )}
        <Button color="#dc3545" title="Cerrar Sesión" onPress={() => cerrarSesión()} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
  text:{
    fontSize: 30,
    marginBottom:20,
    marginLeft: 20,
  },
});

export default Libretas;
