import React, { useContext, useState, useEffect } from "react";
import { View, Button, ScrollView, ActivityIndicator, StyleSheet, TextInput, Text } from "react-native";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario, eliminarNota, editarNota } from "../services/firebase/api";
import { LibretaSeleccionadaContext } from "../context/LibretaSeleccionadaContext";

const Notas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const [libretaSeleccionada, setLibretaSeleccionada] = useContext(LibretaSeleccionadaContext);
  const [nota, setNota] = useState(props.route.params.nota);
  const id_nota = props.route.params.nota.titulo;

  const actualizarNota = () => {
    editarNota(sesionUsuarioContext.id, libretaSeleccionada, id_nota, nota);
    props.navigation.navigate("NotasPorLibreta");
  };

  const eliminarNotaFuncion = () => {
    eliminarNota(sesionUsuarioContext.id, libretaSeleccionada, nota.titulo);
    props.navigation.navigate("NotasPorLibreta");
  };
  return (
    <ScrollView>
      {nota ? (
        <ScrollView style={styles.container}>
          <View>
            <Text style={styles.text}>Nota</Text>
          </View>
          <View style={styles.inputGroup}>
            <TextInput
              placeholder="Título"
              onChangeText={(valor) =>
                setNota({ ...nota, ["titulo"]: valor })
              }
              value={nota.titulo}
            />
          </View>

          <View style={styles.inputGroup}>
            <TextInput
              placeholder="Texto"
              onChangeText={(valor) =>
                setNota({ ...nota, ["texto"]: valor })
              }
              value={nota.texto}
            />
          </View>

          <View style={styles.button}>
            <Button
              color="#198754"
              title="Guardar Nota"
              onPress={() => actualizarNota()}
            />
            </View>
            <View style={styles.button}>
            <Button
              color="#0d6efd"
              title="Eliminar Nota"
              onPress={() => eliminarNotaFuncion()}
            />
          </View>
        </ScrollView>
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#0d6efd" />
        </View>
      )}
      <Button color="#dc3545" title="Cerrar Sesión" onPress={() => cerrarSesión()} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    margin: 6,
  },
  text:{
    fontSize: 30,
    marginBottom:20,
  },
});

export default Notas;
