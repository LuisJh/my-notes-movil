import React, { useContext, useState, useEffect } from "react";
import { View, Button, ScrollView, ActivityIndicator, StyleSheet, Text } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { SesionUsuarioContext } from "../context/SesionUsuarioContext";
import { cerrarSesionUsuario } from "../services/firebase/api";
import { watcherNotasPorLibreta } from "../services/firebase/watcher";
import { LibretaSeleccionadaContext } from "../context/LibretaSeleccionadaContext";
import imagenNota from "../assets/imgNota.png"
import { NotasPorLibretaContext } from "../context/NotasPorLibretaContext";

const Notas = (props) => {
  const cerrarSesión = () => {
    cerrarSesionUsuario();
    props.navigation.navigate("IniciarSesion");
  };

  const sesionUsuarioContext = useContext(SesionUsuarioContext);
  const notasPorLibretaContext = useContext(NotasPorLibretaContext);
  const [libretaSeleccionada, setLibretaSeleccionada] = useContext(LibretaSeleccionadaContext);

  return (
    <ScrollView>
      <View>
        <Text style={styles.text}>Notas de la libreta</Text>
      </View>
      <Button color="#198754" title="Crear Nota" onPress={() => props.navigation.navigate("CrearNota")} />
      {notasPorLibretaContext ? (
        notasPorLibretaContext.length === 0 ? (
          <Text style={styles.noNotas} >No hay notas en esta libreta</Text>
        ) : (
          notasPorLibretaContext.map((nota) => {
            return (
              <ListItem
                key={nota.titulo}
                bottomDivider
                onPress={() => {
                  props.navigation.navigate("Nota", {
                    nota: nota,
                  });
                }}
              >
                <ListItem.Chevron />
                <Avatar
                  source={imagenNota}
                  rounded
                />
                <ListItem.Content>
                  <ListItem.Title>{nota.titulo}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            );
          })
        )
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#0d6efd" />
        </View>
      )}
      <Button color="#dc3545" title="Cerrar Sesión" onPress={() => cerrarSesión()} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  loader: {
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    marginBottom: 7,
  },
  text:{
    fontSize: 30,
    marginBottom:20,
    marginLeft: 20,
  },
  noNotas:{
    fontSize: 20,
    marginBottom:20,
    marginTop:20,
    marginLeft: 20,
  },
});

export default Notas;
